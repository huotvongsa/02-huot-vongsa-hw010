import "./App.css";
import LoginFacebook from "./components/LoginFacebook";
import "bootstrap/dist/css/bootstrap.min.css";
import Menubar from "./components/Menubar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import ViewArticle from "./pages/ViewArticle";
import Article from "./pages/Article";
import Category from "./pages/Category";
import Author from './pages/Author';
import 'moment/locale/km';//to change in khmer;
import LoginGoogle from "./components/LoginGoogle";

function App() {
  return (
    <BrowserRouter>
      {/* <LoginFacebook/> */}
      {/* <LoginGoogle/> */}
      <Menubar/>
      <Container>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/view/:id" component={ViewArticle}/>
          <Route path="/article" component={Article}/>
          <Route path="/category" component={Category}/>
          <Route path="/author" component={Author}/>
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
