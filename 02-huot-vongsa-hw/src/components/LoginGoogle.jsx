import React,{useState} from "react";
import { GoogleLogin } from "react-google-login";

export default function LoginGoogle() {
  const [googleId, setGoogleId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [isLogin, setIsLogin] = useState(false);

  function responseGoogle(response) {
    console.log("RESPONSE:", response.profileObj);
    setIsLogin(true);
    setGoogleId(response.profileObj.googleId);
    setName(response.profileObj.name);
    setEmail(response.profileObj.email);
    setImageUrl(response.profileObj.imageUrl);
  }

  return (
    <div>
      {isLogin ? (
        <div>
          <h1>
            <img src={imageUrl} />
          </h1>
          <h3>{name}</h3>
          <h3>{email}</h3>
        </div>
      ) : (
        <GoogleLogin
          clientId="30110374708-e3r1m0gq73814feq6k2jkis6ucp5335d.apps.googleusercontent.com"
          buttonText="LOGIN WITH GOOGLE"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
        //   cookiePolicy={"single_host_origin"}
        />
      )}
    </div>
  );
}
