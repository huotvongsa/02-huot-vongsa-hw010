import React from "react";
import { Button, Nav, Navbar, Form, FormControl} from 'react-bootstrap'
import { Link, useHistory } from "react-router-dom";

export default function Menubar() {
  const history = useHistory()

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">AMS</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/category">Category</Nav.Link>
          <Nav.Link as={Link} to="/author">Author</Nav.Link>
          {/* <Nav.Link href="#link">Link</Nav.Link> */}
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button onClick={()=>history.push("/article")} variant="outline-success">Add Article</Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
}
