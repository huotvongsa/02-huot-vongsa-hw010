import React from "react";
import FacebookLogin from "react-facebook-login";
import { useEffect, useState } from "react";

export default function LoginFacebook() {
  const [useId, setUseId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [picture, setPicture] = useState("");
  const [isLogin, setIsLogin] = useState(false);

  //Event worked when clicked
  function componentClicked() {
    console.log("Facebook Login clicked...");
  }

  //Response data when logged in
  function responseFacebook(response) {
    console.log("RESPONSE:", response);
    setIsLogin(true);
    setUseId(response.useID);
    setName(response.name);
    setEmail(response.email);
    setPicture(response.picture.data.url);
  }

  return (
    <div>
      {isLogin ? (
        <div>
          <h1>
            <img src={picture} />
          </h1>
          <h3>{name}</h3>
          <h3>{email}</h3>
        </div>
      ) : (
        <FacebookLogin
          appId="518416676018946"
          autoLoad={true}
          fields="name,email,picture"
          cssClass="facebook-style"
          icon={
            <img
              width="60px"
              style={{ paddingRight: "20px" }}
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/B%26W_Facebook_icon.png/768px-B%26W_Facebook_icon.png"
            />
          }
          textButton="LOGIN WITH FACEBOOK"
          onClick={componentClicked}
          callback={responseFacebook}
        />
      )}
    </div>
  );
}
