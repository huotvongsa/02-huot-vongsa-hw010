import React, { useState, useEffect } from "react";
import { Form, Button, Table } from "react-bootstrap";
import {
  addCategory,
  fetchAllCategories,
  fetchCategoryById,
  updateCategoryById,
  deleteCategoryById,
} from "../services/Services";
import { useLocation } from "react-router";
import query from "query-string";
import { useHistory } from "react-router";

export default function Category() {
  const [name, setName] = useState("");
  const { search } = useLocation();
  const { id } = query.parse(search);
  const [category, setCategories] = useState([]);
  const history = useHistory();

  useEffect(async () => {
    const result = await fetchAllCategories();
    console.log("Category:", result);
    setCategories(result);
  }, []);

  //Delete article by ID
  async function onDeleteCategoryById(id) {
    const result = await deleteCategoryById(id);
    const temp = articles.filter((item) => {
      return item._id != id;
    });
    setCategories(temp);
  }

  useEffect(async () => {
    console.log("Search:", typeof search);
    if (search !== "") {
      const result = await fetchCategoryById(id);
      // setArticle(article)
      setName(result.name);
    } else {
      setName("");
    }
  }, [search]);

  //Add/Update article to api
  async function onAddArticle() {
    if (search !== "") {
      const category = {
        name,
      };
      const result = await updateCategoryById(id, name);
      // console.log("updateArticleById2:", result);
    } else {
      const category = {
        name,
      };
      const result = await addCategory(category);
    }
  }

  return (
    <div>
      <h1>{search ? "Update" : "Add"} Category</h1>
      <Form>
        <Form.Control
          placeholder="name"
          style={{ width: "300px" }}
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <Button
          variant="primary"
          onClick={onAddCategory}
          className="my-3"
          variant="primary"
          type="button"
        >
          {search ? "Update" : "Add"}
        </Button>
      </Form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>category</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item._id.slice(0, 8)}</td>
                <td>{item.name}</td>
                <td>
                  <Button
                    onClick={() => history.push(`/category?id=${item._id}`)}
                    variant="warning"
                    className="m-2"
                  >
                    Edit
                  </Button>
                  <Button
                    onClick={() => onDeleteCategoryById(item._id)}
                    variant="danger"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}
