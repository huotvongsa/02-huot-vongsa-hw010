import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { useParams } from "react-router";
import { fetchArticleById } from "../services/Services";

export default function ViewArticle2() {
  const [article, setArticle] = useState({});
  const { id } = useParams();

  useEffect(async () => {
    const result = await fetchArticleById(id);
    setArticle(result);
  }, [id]);

  return (
    <Row className="my-5">
      <Col>
        <h2>{article.title}</h2>
        <h4>{article.description}</h4>
      </Col>
      <Col>
        <div style={{ width: "300px" }}>
          <img width="100%" src={article.image} />
        </div>
      </Col>
    </Row>
  );
}
