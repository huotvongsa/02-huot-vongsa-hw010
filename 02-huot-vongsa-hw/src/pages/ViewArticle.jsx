import React,{useEffect, useState} from 'react'
import { useParams } from 'react-router'
import { fetchArticleById } from '../services/Services';

export default function ViewArticle() {
  const [article, setArticle] = useState({})

  let {id} = useParams()
  console.log("Param:", id);

  useEffect(async() => {
    const result = await fetchArticleById(id);
    setArticle(result)
  }, [id]);

  return (
    <div>
      <h1>{article.title}</h1>
      <img width="400px" src={article.image}/>
      <h4>{article.description}</h4>
    </div>
  )
}
