import React, { useState, useEffect } from "react";
import { Col, Form, Row, Button } from "react-bootstrap";
import {
  addArticle,
  fetchArticleById,
  uploadImage,
  updateArticleById,
} from "../services/Services";
import { useLocation } from "react-router";
import query from 'query-string'

export default function Article() {
  const [myImage, setMyImage] = useState(null);
  const [browsedImage, setBrowsedImage] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  
  const placeholder =
    "https://cdn4.iconfinder.com/data/icons/documents-letters-and-stationery/400/doc-14-512.png";

  const {search} = useLocation()
  console.log("Location:", search);

  let {id} = query.parse(search)
  console.log("id query:", id);

  useEffect(async() => {
    if(search == ""){
      setTitle("")
      setDescription("")
      setBrowsedImage("")
    }else{
      const result = await fetchArticleById(id)
    setTitle(result.title)
    setDescription(result.description)
    setBrowsedImage(result.image)
    }

  }, [id]);


  //Browse image from and add to image tag
  function onBrowsedImage(e) {
    setMyImage(e.target.files[0]);
    setBrowsedImage(URL.createObjectURL(e.target.files[0]));
  }

  //Add/Update article to api with image
  async function onAddArticle() {
    if(search == ""){
      const url = myImage && await uploadImage(myImage);
      const article = {
        title,
        description,
        image: url ? url : placeholder,
      };
      const result = await addArticle(article);
    }else{
      const url = myImage && await uploadImage(myImage);
      
      const article = {
        title,
        description,
        image: url ? url : browsedImage,
      };
      const result = await updateArticleById(id, article)
    }
      
  }

  return (
    <Row className="my-5">
      <Col md={8}>
        <Form>
          <h2>{search?"Update":"Add"} Article</h2>
          <Form.Group controlId="formBasicEmail">
            {/* <Form.Label>Email address</Form.Label> */}
            <Form.Control
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              placeholder="Title"
            />
            <Form.Text className="text-muted">
              {/* We'll never share your email with anyone else. */}
            </Form.Text>
          </Form.Group>
          <Form.Control
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            as="textarea"
            placeholder="Description"
            style={{ height: "100px" }}
          />

          <Button
            onClick={onAddArticle}
            className="my-3"
            variant="primary"
            type="button"
          >
            {search?"Update":"Add"}
          </Button>
        </Form>
      </Col>
      <Col md={4}>
        <div style={{ width: "300px" }}>
          <label htmlFor="myfile">
            <img width="100%" src={browsedImage ? browsedImage : placeholder} />
          </label>
        </div>
        <input
          onChange={onBrowsedImage}
          id="myfile"
          type="file"
          style={{ display: "" }}
        />
      </Col>
    </Row>
  );
}
