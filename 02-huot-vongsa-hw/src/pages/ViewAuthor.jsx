import React,{useEffect, useState} from 'react'
import { useParams } from 'react-router'
import { fetchAuthorById } from '../services/Services';

export default function ViewAuthor() {
  const [author, setAuthor] = useState({})

  let {id} = useParams()
  console.log("Param:", id);

  useEffect(async() => {
    const result = await fetchAuthorById(id);
    setAuthor(result)
  }, [id]);

  return (
    <div>
      <h1>{author.name}</h1>
      <h4>{author.email}</h4>
      <img width="400px" src={author.image}/>
      
    </div>
  )
}
