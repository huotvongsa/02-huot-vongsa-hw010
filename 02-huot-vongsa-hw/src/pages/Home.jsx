import React, { useEffect, useState } from "react";
import { Table, Button } from "react-bootstrap";
import Moment from "react-moment";
import { useHistory } from "react-router";
import { deleteAritcleById, fetchAllArticles } from "../services/Services";

export default function Home() {
  const [articles, setArticles] = useState([]);
  const history = useHistory();
  const [date, setDate] = useState(Date.now())

  useEffect(async () => {
    const result = await fetchAllArticles();
    console.log("Articles:", result);
    setArticles(result);
  }, []);

  //Delete article by ID
  async function onDeleteArticleById(id){
    const result = await deleteAritcleById(id);
    const temp = articles.filter(item=>{
      return item._id != id
    })
    setArticles(temp)
  }

  return (
    <>
    <h1 className="text-center my-3" style={headerStyle}>Article Management System</h1>
    <Moment date={date} fromNow ago locale="km" />
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Description</th>
          <th>Image</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {articles.map((item, index) => {
          return (
            <tr key={index}>
              <td>{item._id.slice(0,8)}</td>
              <td>{item.title}</td>
              <td>{item.description}</td>
              <td>
                <img width="200px" src={item.image} />
              </td>
              <td>
                <Button onClick={()=>history.push(`/view/${item._id}`)} variant="primary">View</Button>{' '}
                <Button onClick={()=>history.push(`/article?id=${item._id}`)} variant="warning">Edit</Button>{' '}
                <Button onClick={()=>onDeleteArticleById(item._id)} variant="danger">Delete</Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
    </>
  );
}

const headerStyle = {
  color: 'white',
  backgroundColor: 'green',
}