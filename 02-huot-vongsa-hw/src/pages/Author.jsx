import React, { useState, useEffect } from "react";
import { Col, Form, Row, Button, Table } from "react-bootstrap";
import {
  addAuthor,
  fetchAuthorById,
  uploadImageAuthor,
  updateAuthorById,
  deleteAuthorById,
  fetchAllAuthors,
} from "../services/Services";
import { useLocation } from "react-router";
import query from "query-string";
import { useHistory } from "react-router";


export default function Author() {
  const [myImage, setMyImage] = useState(null);
  const [browsedImage, setBrowsedImage] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [authors, setAuthors] = useState([]);
  const history = useHistory();

  const placeholder =
    "https://cdn4.iconfinder.com/data/icons/documents-letters-and-stationery/400/doc-14-512.png";

  const { search } = useLocation();
  console.log("Location:", search);

  let { id } = query.parse(search);
  console.log("id query:", id);

  useEffect(async () => {
    if (search == "") {
      setName("");
      setEmail("");
      setBrowsedImage("");
    } else {
      const result = await fetchAuthorById(id);
      setName(result.name);
      setEmail(result.email);
      setBrowsedImage(result.image);
    }
  }, [id]);

  //Browse image from and add to image tag
  function onBrowsedImage(e) {
    setMyImage(e.target.files[0]);
    setBrowsedImage(URL.createObjectURL(e.target.files[0]));
  }

  //Add/Update author to api with image
  async function onAddAuthor() {
    if (search == "") {
      const url = myImage && (await uploadImageAuthor(myImage));
      const author = {
        name,
        email,
        image: url ? url : placeholder,
      };
      const result = await addAuthor(author);
    } else {
      const url = myImage && (await uploadImageAuthor(myImage));

      const author = {
        name,
        email,
        image: url ? url : browsedImage,
      };
      const result = await updateAuthorById(id, author);
    }
  }

  useEffect(async () => {
    const result = await fetchAllAuthors();
    console.log("Authors:", result);
    setAuthors(result);
  }, []);

  //Delete author by ID
  async function onDeleteAuthorById(id){
    const result = await deleteAuthorById(id);
    const temp = authors.filter(item=>{
      return item._id != id
    })
    setAuthors(temp)
  }

  return (
    <div>
      <Row className="my-5">
        <Col md={8}>
          <Form>
            <h2>{search ? "Update" : "Add"} Author</h2>
            <Form.Group controlId="formBasicName">
              <Form.Label>Author Name</Form.Label>
              <Form.Control
               type="text"
               value={name}
               onChange={(e) => setName(e.target.value)}
               placeholder="author name"
              />
              <Form.Text className="text-muted">
                
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                placeholder="Email"
                />
            </Form.Group>
            

            <Button
               onClick={onAddAuthor}
               className="my-3"
               variant="primary"
               type="button"
            >
              {search ? "Update" : "Add"}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
          <div style={{ width: "300px" }}>
            <label htmlFor="myfile">
              <img
                width="100%"
                src={browsedImage ? browsedImage : placeholder}
              />
            </label>
          </div>
          <input
           onChange={onBrowsedImage}
           id="myfile"
           type="file"
           style={{ display: "" }}
          />
        </Col>
      </Row>
      <Row>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {authors.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item._id}</td>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td><img width="200px" src={item.image} /></td>
                  <td>
                    <Button
                        onClick={() => history.push(`/view/${item._id}`)}
                      variant="primary"
                    >
                      View
                    </Button>{" "}
                    <Button
                        onClick={() => history.push(`/author?id=${item._id}`)}
                      variant="warning"
                    >
                      Edit
                    </Button>{" "}
                    <Button
                        onClick={() => onDeleteAuthorById(item._id)}
                      variant="danger"
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Row>
    </div>
  );
}
